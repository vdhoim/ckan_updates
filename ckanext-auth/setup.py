from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
    name='ckanext-auth',
    version=version,
    description="CKAN authorization extension",
    long_description='''
    ''',
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Alex Siaba',
    author_email='alex.siaba@vdh.virginia.gov',
    url='',
    license='',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.auth'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
        'python-memcached==1.48',
    ],
    entry_points='''
        [ckan.plugins]
        auth=ckanext.auth.plugin:AuthPlugin
    ''',
)