import ckan.plugins as p
import ckan.model as model


class AuthPlugin(p.SingletonPlugin):

    p.implements(p.IAuthenticator, inherit=True)


    def identify(self):
        pass

    def login(self):
        pass

    def logout(self):
        pass
