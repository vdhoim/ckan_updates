#!/usr/bin/env bash

#activate env
. /usr/lib/ckan/default/bin/activate

#develop plugins
cd /usr/lib/ckan/default/src/ckanext-vdh_template
python setup.py develop

cd /usr/lib/ckan/default/src/ckanext-vdh_dataset
python setup.py develop