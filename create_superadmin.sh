#!/usr/bin/env bash


. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan

username='harvest'
password=$(pwgen -N 1 -s)
email='example@example.com'

for i in "$@"
do
    case $i in
        -u=*|--username=*)
        username="${i#*=}"
        shift
        ;;
        -p=*|--password=*)
        password="${i#*=}"
        shift
        ;;
        -e=*|--email=*)
        email="${i#*=}"
        shift
        ;;
    esac
done


user_string=$(paster user $username -c /etc/ckan/default/ckan.ini)
user_exists=$(echo $user_string | grep -c "<User id=")
if [ $user_exists -eq 1 ]; then
        echo "this user already exists...";
else
        user=$(paster user add $username email=$email password=$password -c /etc/ckan/default/ckan.ini)
        if [[ $user =~ ^.*apikey.*$ ]]; then
                paster sysadmin add $username -c /etc/ckan/default/ckan.ini &> /dev/null
                apikey=$(echo $user | grep -P -o "(?<=apikey\'\:\su\')[a-zA-Z0-9\-]*")
                echo $apikey
        else
                echo "this user hasn't been created...";
        fi
fi