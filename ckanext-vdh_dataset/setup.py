from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
    name='ckanext-vdh_dataset',
    version=version,
    description="Modifications to CKAN dataset",
    long_description='''
    ''',
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Alex Siaba',
    author_email='alex.siaba@vdh.virginia.gov',
    url='',
    license='',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.vdh_dataset'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points='''
        [ckan.plugins]
        vdh_dataset=ckanext.vdh_dataset.plugin:Vdh_DatasetPlugin
    ''',
)
