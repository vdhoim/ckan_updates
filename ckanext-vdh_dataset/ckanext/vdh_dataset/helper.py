import solr, socket, hashlib, json
import ckan.logic as logic
import ckan.lib.dictization.model_dictize as model_dictize
import ckan.lib.navl.dictization_functions as dictization_functions
import ckan.lib.search as search
import ckan.plugins as plugins
import ckan.authz as authz
from ckan.lib.search.common import make_connection, SearchIndexError
from ckan.plugins import toolkit
from pylons import config
from ckan.logic.action.get import package_show


def vdh_package_show(context, data_dict):
    context['ignore_auth'] = True
    return package_show(context, data_dict)


def vdh_package_search(context, data_dict):
    """
    This is fully copied function package_search from logic.action.get module
    :param context:
    :param data_dict:
    :return:
    """

    schema = (context.get('schema') or
              logic.schema.default_package_search_schema())
    data_dict, errors = dictization_functions.validate(data_dict, schema, context)
    # put the extras back into the data_dict so that the search can
    # report needless parameters
    data_dict.update(data_dict.get('__extras', {}))
    data_dict.pop('__extras', None)
    if errors:
        raise logic.ValidationError(errors)

    model = context['model']
    session = context['session']
    user = context['user']

    logic.check_access('package_search', context, data_dict)

    # Move ext_ params to extras and remove them from the root of the search
    # params, so they don't cause and error
    data_dict['extras'] = data_dict.get('extras', {})
    for key in [key for key in data_dict.keys() if key.startswith('ext_')]:
        data_dict['extras'][key] = data_dict.pop(key)

    # check if some extension needs to modify the search params
    for item in plugins.PluginImplementations(plugins.IPackageController):
        data_dict = item.before_search(data_dict)

    # the extension may have decided that it is not necessary to perform
    # the query
    abort = data_dict.get('abort_search', False)

    if data_dict.get('sort') in (None, 'rank'):
        data_dict['sort'] = 'score desc, metadata_modified desc'

    results = []
    if not abort:
        data_source = 'data_dict' if data_dict.get('use_default_schema',
            False) else 'validated_data_dict'
        # return a list of package ids
        data_dict['fl'] = 'id {0}'.format(data_source)

        # If this query hasn't come from a controller that has set this flag
        # then we should remove any mention of capacity from the fq and
        # instead set it to only retrieve public datasets
        fq = data_dict.get('fq', '')
        if not context.get('ignore_capacity_check', False):
            fq = ' '.join(p for p in fq.split(' ')
                            if not 'capacity:' in p)
            data_dict['fq'] = fq + ' capacity:"public"'

        # Solr doesn't need 'include_drafts`, so pop it.
        include_drafts = data_dict.pop('include_drafts', False)
        fq = data_dict.get('fq', '')
        if include_drafts:
            user_id = authz.get_user_id_for_username(user, allow_none=True)
            if authz.is_sysadmin(user):
                data_dict['fq'] = fq + ' +state:(active OR draft)'
            elif user_id:
                # Query to return all active datasets, and all draft datasets
                # for this user.
                data_dict['fq'] = fq + \
                    ' ((creator_user_id:{0} AND +state:(draft OR active))' \
                    ' OR state:active)'.format(user_id)
        elif not authz.is_sysadmin(user):
            data_dict['fq'] = fq + ' +state:active'

        # Pop these ones as Solr does not need them
        extras = data_dict.pop('extras', None)

        query = search.query_for(model.Package)

        query.run(data_dict)

        # Add them back so extensions can use them on after_search
        data_dict['extras'] = extras

        for package in query.results:
            # get the package object
            package, package_dict = package['id'], package.get(data_source)

            pkg_query = session.query(model.Package)\
                .filter(model.Package.id == package)\
                .filter(model.Package.state.in_((u'active', u'draft')))
            pkg = pkg_query.first()

            ## if the index has got a package that is not in ckan then
            ## ignore it.
            if not pkg:
                results.append(json.loads(package_dict))
                # log.warning('package %s in index but not in database' % package)
                continue
            ## use data in search index if there
            if package_dict:
                ## the package_dict still needs translating when being viewed
                package_dict = json.loads(package_dict)
                if context.get('for_view'):
                    for item in plugins.PluginImplementations(
                            plugins.IPackageController):
                        package_dict = item.before_view(package_dict)
                results.append(package_dict)
            else:
                results.append(model_dictize.package_dictize(pkg,context))

        count = query.count
        facets = query.facets
    else:
        count = 0
        facets = {}
        results = []

    search_results = {
        'count': count,
        'facets': facets,
        'results': results,
        'sort': data_dict['sort']
    }

    # Transform facets into a more useful data structure.
    restructured_facets = {}
    for key, value in facets.items():
        restructured_facets[key] = {
                'title': key,
                'items': []
                }
        for key_, value_ in value.items():
            new_facet_dict = {}
            new_facet_dict['name'] = key_
            if key in ('groups', 'organization'):
                group = model.Group.get(key_)
                if group:
                    new_facet_dict['display_name'] = group.display_name
                else:
                    new_facet_dict['display_name'] = key_
            elif key == 'license_id':
                license = model.Package.get_license_register().get(key_)
                if license:
                    new_facet_dict['display_name'] = license.title
                else:
                    new_facet_dict['display_name'] = key_
            else:
                new_facet_dict['display_name'] = key_
            new_facet_dict['count'] = value_
            restructured_facets[key]['items'].append(new_facet_dict)
    search_results['search_facets'] = restructured_facets

    # check if some extension needs to modify the search results
    for item in plugins.PluginImplementations(plugins.IPackageController):
        search_results = item.after_search(search_results,data_dict)

    # After extensions have had a chance to modify the facets, sort them by
    # display name.
    for facet in search_results['search_facets']:
        search_results['search_facets'][facet]['items'] = sorted(
                search_results['search_facets'][facet]['items'],
                key=lambda facet: facet['display_name'], reverse=True)

    return search_results


@toolkit.side_effect_free
def vdh_wp_post_remove(context,data_dict=None):
    """
    Removes wp entry from solr index
    :param context:
    :param data_dict: dict, params:
        :id - wp post id
    :return:
    """
    logic.check_access('package_create', context, data_dict)

    errors = []
    if not data_dict.get('id', False):
        raise SearchIndexError('"%s" values must be provided' % ','.join(errors))

    try:
        conn = make_connection()
        index_id = hashlib.md5('%s%s' % (data_dict.get('id', '0'), config.get('ckan.site_id'))).hexdigest()
        conn.delete(index_id)
        conn.commit()
    except solr.core.SolrException, e:
        msg = 'Solr returned an error: {0} {1} - {2}'.format(
            e.httpcode, e.reason, e.body[:1000] # limit huge responses
        )
        raise SearchIndexError(msg)
    except socket.error, e:
        err = 'Could not connect to Solr using {0}: {1}'.format(conn.url, str(e))
        raise SearchIndexError(err)
    finally:
        conn.close()

    return dict(index_id=index_id, id=data_dict.get('id', '0'))

@toolkit.side_effect_free
def vdh_wp_post_add(context,data_dict=None):

    """
    Adds wp entry to solr index
    :param context:
    :param data_dict: dict, params:
        :id - wp post id
        :url - wp post url
        ;title - wp post title
        :notes - wp post body
    :return:
    """

    logic.check_access('package_create', context, data_dict)

    errors = []
    if not data_dict.get('id', False):
        errors.append('id')
    if not data_dict.get('url', False):
        errors.append('url')
    if not data_dict.get('title', False):
        errors.append('title')
    if not data_dict.get('notes', False):
        errors.append('notes')

    if len(errors) > 0:
        raise SearchIndexError('"%s" values must be provided' % ','.join(errors))

    data_dict['index_id'] = hashlib.md5('%s%s' % (data_dict['id'],config.get('ckan.site_id'))).hexdigest()
    data_dict['site_id'] = config.get('ckan.site_id')
    data_dict['dataset_type'] = 'dataset'
    data_dict['capacity'] = 'public'
    data_dict['data_maturity'] = 'public'
    data_dict['state'] = 'active'


    validated_data_dict, errors = dictization_functions.validate(data_dict, {
        'id':[],'title':[],'url':[],'notes':[],
    })

    data_dict['validated_data_dict'] = json.dumps(validated_data_dict)

    try:
        conn = make_connection()
        add_result = conn.add_many([data_dict], _commit=True)
    except solr.core.SolrException, e:
        msg = 'Solr returned an error: {0} {1} - {2}'.format(
            e.httpcode, e.reason, e.body[:1000] # limit huge responses
        )
        raise SearchIndexError(msg)
    except socket.error, e:
        err = 'Could not connect to Solr using {0}: {1}'.format(conn.url, str(e))
        raise SearchIndexError(err)
    finally:
        conn.close()

    ret = json.loads(data_dict['validated_data_dict'])
    ret['add_result'] = add_result
    return ret

