from ckan import plugins as p
from ckan.plugins import toolkit as t
from ckan.logic.schema import default_create_package_schema, \
                              default_update_package_schema, \
                              default_show_package_schema
from pylons import config
from helper import *




class Vdh_DatasetPlugin(p.SingletonPlugin, t.DefaultDatasetForm):

    p.implements(p.IDatasetForm, inherit=True)
    p.implements(p.IActions, inherit=True)

    data_maturity_levels = dict(
        public=0,
        internal=1,
        provisional=2,
        experimental=3
    )

    def create_package_schema(self):
        schema = default_create_package_schema()
        schema.update({
            'data_maturity': [self._data_maturity_validator, t.get_converter('convert_to_extras')],
            'documentation': [t.get_validator('ignore_empty'), t.get_converter('convert_to_extras')],
            'expert_text': [t.get_validator('ignore_empty'), t.get_converter('convert_to_extras')],
            'private': [self._private_validator]
        })
        return schema

    def update_package_schema(self):
        schema = default_update_package_schema()
        schema.update({
            'data_maturity': [self._data_maturity_validator, t.get_converter('convert_to_extras')],
            'documentation': [t.get_validator('ignore_empty'), t.get_converter('convert_to_extras')],
            'expert_text': [t.get_validator('ignore_empty'), t.get_converter('convert_to_extras')],
            'private' : [self._private_validator]
        })
        return schema

    def show_package_schema(self):
        schema = default_show_package_schema()
        schema.update({
            'data_maturity': [t.get_converter('convert_from_extras'), ],
            'documentation': [t.get_converter('convert_from_extras')],
            'expert_text': [t.get_converter('convert_from_extras')],
        })
        return schema

    def is_fallback(self):
        return True

    def package_types(self):
        return []

    def _data_maturity_validator(self, key, data, errors, context):
        data_maturity = data.get(key, 'experimental')
        if not data_maturity in self.data_maturity_levels:
            data_maturity = 'experimental'
            data[key] = data_maturity

    def _private_validator(self, key, data, errors, context):
        data_maturity = data.get(('data_maturity', ), 'experimental')
        if not data_maturity in self.data_maturity_levels:
            data_maturity = 'experimental'
        data[key] = self.data_maturity_levels[data_maturity] > 0

    def get_actions(self):
        return {
            'package_show': vdh_package_show,
            'package_search': self._vdh_package_search(),
            'wp_post_add': vdh_wp_post_add,
            'wp_post_remove': vdh_wp_post_remove
        }

    def _vdh_package_search(self):
        function = vdh_package_search
        data_maturity = config.get('data_maturity', 0)
        if data_maturity > 0:
            def function(context, data_dict):
                context['ignore_capacity_check'] = True
                data_dict['fq'] = 'data_maturity:/({})/'.format(self._max_data_maturity(int(data_maturity)))
                return vdh_package_search(context, data_dict)
        return function

    def _max_data_maturity(self, data_maturity):
        return ''.join('%s|' % name for name, level in self.data_maturity_levels.iteritems() if level <= data_maturity)[:-1]