from ckan import plugins
import ckan.plugins.toolkit as toolkit
from ckan.lib import jinja_extensions
from ckan.lib.helpers import nav_link, build_nav_icon


from helper import Vdh_TemplateHelper, vdh_package_list


class Vdh_TemplatePlugin(plugins.SingletonPlugin):

    plugins.implements(plugins.IActions)
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IMiddleware)
    plugins.implements(plugins.IRoutes)
    plugins.implements(plugins.ITemplateHelpers)

    replace = dict(
        original=['-alt', '-time', '-picture'],
        replace = ['', '-clock-o', '-picture-o'],
    )

    def get_actions(self):
        return {
            'vdh_package_list': vdh_package_list
        }

    def before_map(self, map):
        map.redirect('/', '/dataset')
        return map

    def after_map(self, map):
        return map

    def make_middleware(self, app, config):
        config['pylons.app_globals'].jinja_env.add_extension(self.LinkForExtension)
        return app

    def make_error_log_middleware(self, app, config):
        return app

    def update_config(self, config):
        toolkit.add_template_directory(config, 'templates') #overriding native templates

    def get_helpers(self):
        helper = Vdh_TemplateHelper()
        return {
            'header': helper,
            'build_nav_icon': self.build_nav_icon,
            'main_menu': helper.main_menu,
            'tableau': helper.tableau,
            'wp_post_link': helper.wp_post_link,
            'wp_expert_text': helper.wp_expert_text
        }

    def build_nav_icon(self, menu_item, title, **kwargs):
        return self.to_bs_three(self.replace, build_nav_icon(menu_item, title, **kwargs))

    @staticmethod
    def to_bs_three(replace, link):
        for i, r in enumerate(replace['original']):
            link = link.replace(r, replace['replace'][i])
        return link



    class LinkForExtension(jinja_extensions.BaseExtension):
        ''' Custom link_for tag

        {% link_for <params> %}

        see lib.helpers.nav_link() for more details.
        '''

        tags = set(['link_for'])

        @classmethod
        def _call(cls, args, kwargs):
            return cls.to_bs_three(cls.replace, nav_link(*args, **kwargs))

    LinkForExtension.replace = replace
    LinkForExtension.to_bs_three = to_bs_three