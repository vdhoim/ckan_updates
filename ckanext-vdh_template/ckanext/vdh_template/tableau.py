from template import Vdh_Template

from pprint import pprint

class Vdh_Tableau():

    _url = ''
    _host_url = ''
    _name = ''
    _parameters = dict()


    def __init__(self, url):
        self._url = url
        self._template_generator = Vdh_Template('templates/tableau')
        self._prepare()

    def _prepare(self):
        host_and_name_plus_params = self._url.split('views/')
        self._host_url = host_and_name_plus_params[0]
        name_and_params = host_and_name_plus_params[1].split('?')
        self._name = name_and_params[0]
        params = name_and_params[1].split('&')
        self._parameters = {param.split('=')[0][1:]: param.split('=')[1] for param in params if param.split('=')[0][1:] != 'embed'}


    def get_html(self):
        return self._template_generator.get_html(
            'tableau.html',
            dict(
                host_url=self._host_url,
                url=self._url,
                name=self._name,
                parameters=self._parameters
            ))