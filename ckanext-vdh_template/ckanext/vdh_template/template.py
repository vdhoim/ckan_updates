import os
from jinja2 import Environment, FileSystemLoader

class Vdh_Template:

    def __init__(self, templates_folder):
        self._dir =  os.path.join(os.path.dirname(os.path.realpath(__file__)), templates_folder)

    def _get_environment(self, dir):
        loader = FileSystemLoader(dir)
        return Environment(loader=loader)

    def _get_template(self, dir, template):
        env = self._get_environment(dir)
        return env.get_template(template)

    def get_html(self, template, variables):
        t = self._get_template(self._dir, template)
        return t.render(vars=variables)