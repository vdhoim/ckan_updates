import requests, sqlalchemy
from ckan import logic
from pylons import config
from ckan import plugins
from main_menu import Vdh_TemplateMainMenu
from tableau import Vdh_Tableau
from template import Vdh_Template



class Vdh_TemplateHelper:

    def __init__(self):
        self.wordpress_host = config.get('wordpress.host', 'http://informatics.vdh.virginia.gov')
        data = self._caller('%s/api/header/' % self.wordpress_host).json() #we could cache values here
        self.website_url = self.wordpress_host
        self.topmenu_links = data['menus'].get('top', [])
        self.footer_links = data['menus'].get('footer-links', [])
        self.logo = dict(src=data['blog_options'].get('logo', ''),alt=data['blog_options'].get('description', ''))
        self._main_menu_object = Vdh_TemplateMainMenu(data['blog_options'])


    def main_menu(self):
        data = self._caller('%s/api/header/menu/primary' % self.wordpress_host).json()
        return plugins.toolkit.literal(self._main_menu_object.get_html(data['menus'].get('primary', [])))

    def tableau(self, url):
        t = Vdh_Tableau(url)
        return plugins.toolkit.literal(t.get_html())

    def wp_post_link(self, package):
        t = Vdh_Template('templates/snippets')
        return plugins.toolkit.literal(t.get_html('wp_post_link.html', package))

    def wp_expert_text(self, package):
        t = Vdh_Template('templates/snippets')
        data = self._caller('%s/api/post?id=%s' % (self.wordpress_host, package.get('id', ''))).json()
        html = ''
        if data.get('success', False):
            html = plugins.toolkit.literal(t.get_html('wp_expert_text.html', data))
        return html

    def _caller(self, url, data=None):
        if data:
            return requests.post(url, data)
        else:
            return requests.get(url)

    @staticmethod
    def template_options():
        return dict(template_options=True)


@plugins.toolkit.side_effect_free
def vdh_package_list(context, data_dict):
    """f
    Basically, overriden actions.get.package_list
    :param context:
    :param data_dict:
    :return: list of packages
    """

    model = context["model"]
    api = context.get("api_version", 1)

    logic.check_access('package_list', context, data_dict)

    package_table = model.package_table

    col = (package_table.c.id
           if api == 2 else package_table.c.title)

    order_by = package_table.c.revision_id.desc()

    query = sqlalchemy.select([col])
    query = query.where(sqlalchemy.and_(
        package_table.c.state == 'active',
        package_table.c.type != 'harvest'
    ))
    query = query.order_by(order_by)

    limit = data_dict.get('limit')
    if limit:
        query = query.limit(limit)

    offset = data_dict.get('offset')
    if offset:
        query = query.offset(offset)

    ## Returns the first field in each result record
    return [r[0] for r in query.execute()]