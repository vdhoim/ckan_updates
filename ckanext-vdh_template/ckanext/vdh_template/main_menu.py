from ckan import plugins
from template import Vdh_Template

class Vdh_TemplateMainMenu():

    _html = ''
    _blog_options = dict()

    def __init__(self, blog_options):
        self._blog_options = blog_options
        self._template_generator = Vdh_Template('templates/menu')

    def _add_ckan_header(self, data):
        def _add_ids(l, p_id):
            id = p_id * 100
            for i,v in enumerate(l):
                l[i]['id'] = --id
                l[i]['parent'] = p_id
                try:
                    l[i]['children'] = _add_ids(v['children'], id) if l[i]['children'] else []
                except KeyError:
                    continue
            return l

        ckan_header = self._get_header_menu_data()
        for i, menu_item in enumerate(data):
            if menu_item['url'] == self._blog_options['ckan_url']:
                data[i]['children'] = _add_ids(ckan_header, menu_item['id'])

        return data


    def _get_header_menu_data(self):
        data = [
            self._build_set("Datasets", "#", self._get_children('vdh_package_list', dict(limit=3), 'dataset')),
            self._build_set("Organizations", "#", self._get_children('organization_list',{}, 'organization')),
            self._build_set("Groups", "#", self._get_children('group_list',{}, 'group')),
        ]
        return data

    @staticmethod
    def _build_set(name,url,children):
        return dict(name=name, url=url, children=children)

    def _get_children(self, action, options, type):
        context = dict(user=None) #bug in ckan.logic.action.get line 239
        children = plugins.toolkit.get_action(action)(context, options)
        return [self._build_set(child.replace('-', ' ').title(),'/%s/%s'%(type,child),[]) for child in children]

    def add_children(self, children):
        return self._template_generator.get_html(template='child_menu_item.html', variables=dict(children=children, menu=self))

    def add_item(self, item):
        site_partitions = ['Datasets', 'Organizations', 'Groups']
        if item['name'] in site_partitions:
            item['link'] = '/%s/'%item['name'].lower()[0:-1]
        return self._template_generator.get_html(template='menu_item.html', variables=dict(item=item, menu=self))

    def get_html(self, data):
        self._html = ''
        for item in self._add_ckan_header(data):
            self._html += self.add_item(item)
        return self._html