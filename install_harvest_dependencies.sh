#!/usr/bin/env bash

#activate env
. /usr/lib/ckan/default/bin/activate

#install official harvester
pip install -e git+https://github.com/okfn/ckanext-harvest.git#egg=ckanext-harvest
pip install -r /usr/lib/ckan/default/src/ckanext-harvest/pip-requirements.txt
pip install -r /usr/lib/ckan/default/src/ckanext-harvest/dev-requirements.txt
paster --plugin=ckanext-harvest harvester initdb --config=/etc/ckan/default/ckan.ini
