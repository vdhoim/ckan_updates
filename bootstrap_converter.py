import os, re
from shutil import copyfile

class Runner:

    def __init__(self, directory):
        self.rel_directory = directory
        self.directory = os.path.join(os.path.dirname(os.path.realpath(__file__)) , directory)

    def run(self, directory=None):
        if not directory:
            directory = self.directory
        for resource in os.listdir(directory):
            res = os.path.join(directory, resource)
            if resource.endswith('.html'):
                f = FileProcessor(res)
                f.process(self.get_destination(directory))
            elif os.path.isdir(res):
                pass
                self.run(res)

    def get_destination(self, directory):
        if directory == os.path.realpath(self.rel_directory):
            return ''
        return directory.replace(os.path.realpath(self.rel_directory) + '/', '')



class FileProcessor:

    _dest = os.path.join(os.path.dirname(os.path.realpath(__file__)) , 'ckanext-vdh_template/ckanext/vdh_template/templates')

    def __init__(self, file):
        self.file = file

    def process(self, dest):
        dst = os.path.join(self._get_directory(dest), os.path.basename(self.file))
        if not os.path.exists(dst):
            self.write_to_file(dst)


    def write_to_file(self, dst):
        with open(self.file, 'r') as r, open(dst, 'w') as w:
            for line in r:
                l = BootstrapConverter(line)
                w.write(l.convert())


    def _get_directory(self, d):
        directory = os.path.join(self._dest, d)
        if not os.path.exists(directory):
            os.makedirs(directory, 0755)
        return directory


class BootstrapConverter:

    _equivalent = {
        'bs2': [r"row-fluid",                   r"span(\d+)",               r"offset(\d+)",                 r"brand",                   r"navbar([\w\s\-]*)nav",
                r"nav-collapse",                r"nav-toggle",              r"btn-navbar",                  r"hero-unit",               r'-sign"',
                r"btn",                         r"btn-mini",                r"btn-small",                   r"btn-large",               r"alert-error",
                r"visible-phone",               r"visible-tablet",          r"visible-desktop",             r"hidden-phone",            r"hidden-tablet",
                r"hidden-desktop",              r"input-block-level",       r"control-group",               r"(warning|error|success])",r"checkbox([\w\s\-]*)inline",
                r"radio([\w\s\-]*)inline",      r"input\-prepend",          r"input\-append",               r"add-on",                  r"img-polaroid",
                r"unstyled",                    r"inline",                  r"muted",                       r"label",                   r"label-important",
                r"text-error",                  r"table([\w\s\-]*)error",   r"bar",                         r"bar-(\w*)",               r"accordion",
                r"accordion-group",             r"accordion-heading",       r"accordion-body",              r"accordion-inner",         r"dropdown btn",
                r"icon-time",                   r"icon-picture",            r"icon-share-alt",
        ],

        'bs3': [r"row",                 r"col-md-\1",       r"col-md-offset-\1",        r"navbar-brand",        r"navbar-nav \1",
                r"navbar-collapse",     r"navbar-toggle",   r"navbar-btn",              r"jumbotron",           r'"',
                r"btn btn-default",     r"btn-xs",          r"btn-sm",                  r"btn-lg",              r"alert-danger",
                r"visible-xs",          r"visible-sm",      r"visible-md visible-lg",   r"hidden-xs",           r"hidden-sm",
                r"hidden-md hidden-lg", r"form-control",    r"form-group",              r"form-group.has-\1",   r"checkbox-inline \1",
                r"radio-inline \1",     r"input-group",     r"input-group",             r"input-group-addon",   r"img-thumbnail",
                r"list-unstyled",       r"list-inline",     r"text-muted",              r"label label-default", r"label-danger",
                r"text-danger",         r"table danger \1", r"progress-bar",            r"progress-bar-\1",     r"panel-group",
                r"panel panel-default", r"panel-heading",   r"panel-collapse",          r"panel-body",          r"dropdown",
                r"icon-clock-o"         r"icon-picture-o"   r"icon-share"
        ]
    }



    _removed = ["form-actions", r"form-search", r"input-mini", r"input-small", r"input-medium", r"input-large", r"input-xlarge", r"input-xxlarge", r"input-block-level", r"btn-inverse", r"row-fluid", r"controls", r"controls-row", r"navbar-inner", r"divider-vertical", r"dropdown-submenu", r"tabs-left", r"tabs-right", r"tabs-below", r"pill-content", r"pill-pane", r"nav-list", r"nav-header", r"help-inline", r"progress-info", r"progress-success", r"progress-warning", r"progress-danger", r"icon-medium"]

    _noticed_bugs = ["progress-progress-", ]

    def __init__(self, line):
        self.line = line

    def convert(self):
        self._parse()
        return self.line

    def _parse(self):
        matches = list(re.finditer(r'(class="[^\"]+")', self.line))
        for match in matches:
            m = match.group()
            cls = self._remove(m)
            cls = self._replace(cls)
            cls = self._remove(cls, True)
            self.line = re.sub(m, cls, self.line)

    def _remove(self, cls, is_bug=False):
        for r in self._noticed_bugs if is_bug else self._removed:
            cls = re.sub(re.compile(r), '', cls)
        return cls

    def _replace(self, cls):
        for i,r in enumerate(self._equivalent['bs2']):
            try:
                cls = re.sub(re.compile(r), self._equivalent['bs3'][i], cls)
            except IndexError:
                print i
        return cls





runner = Runner('ckan/ckan/templates')
runner.run()